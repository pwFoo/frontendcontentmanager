ToDo: readme / wiki / docs

Simple usage:
```
// load FCM
$fcm = $modules->get('FrontendContentManager');

// add a page to given parent with parent template
$output = $fcm->renderAdd($parentPage);

// edit given page
$output = $fcm->renderEdit($pageToEdit);

// echo jsConfig inside the html head element
echo $fcm->JsConfig(); 

// outpunt needed scripts for inputfield modules, ... inside your html head element
foreach ($config->styles as $file) { echo "<link type='text/css' href='$file' rel='stylesheet' />\n"; } 
foreach ($config->scripts as $file) { echo "<script type='text/javascript' src='$file'></script>\n"; }

// echo $output inside your template / html body element
echo $output;
```

Advanced usage have additional params ($redirect, $template, skip fields, use admin form styles)...
I'll write a documentation soon... at the moment you have to take a look into the source code or be patient ;)